import java.util.Scanner;
public class Aula{
    public static Scanner leia = new Scanner(System.in);
    public static boolean login(String user,String senha){
        if(user.equals("admin") && senha.equals("123456")){
            return true;
        }else{
            return false;
        }
    }
    public static void exibirMenu(String usuario){
       if(usuario.equals("admin")){ 
            System.out.println("------------------");
            System.out.println("1 - Cadastrar");
            System.out.println("2 - Editar");
            System.out.println("3 - Listar");
            System.out.println("4 - Excluir");
            System.out.println("0 - Sair");
       }else{
           System.out.println("Você não está autorizado a usar o sistema.");
       }
    }
    public static void main(String[] args) {
        String nm;
        String snh;
        System.out.print("Digite seu nome de usuário: ");
        nm = leia.next();
        System.out.println("Digite sua senha: ");
        snh = leia.next();
        if(login(nm,snh)){
            exibirMenu(nm);
        }else{
            System.out.println("Você não pode usar o sistema.");
        }
    }
}








